
from django.urls import path
from .views import home, contacto, catalogo, mi_cuenta, nosotros, agregar_producto, modificar_producto, listar_productos, eliminar_producto

urlpatterns = [
    path('', home , name="home"),
    path('catalogo/', catalogo , name="catalogo"),
    path('contacto/', contacto , name="contacto"),
    path('mi_cuenta/', mi_cuenta , name="mi_cuenta"),
    path('nosotros/', nosotros , name="nosotros"),
    path('agregar-producto/', agregar_producto, name="agregar_producto"),
    path('modificar-producto/<id>/', modificar_producto, name="modificar_producto"),
    path('listar-productos/', listar_productos, name="listar_productos"),
    path('eliminar-producto/<id>/', eliminar_producto, name="eliminar_producto"),
]
